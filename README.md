# tictrack

A tool to track your topics speaking time at meetings. Formerly designed to be able to reduce our daily standup meetings which were too long.

It is really simple, that for now it does not even relies on a backend server. Data (participants) are stored in the browser localStorage.

# Roadmap at a glance

* [x] Keep the participants list from one session to another one
* [ ] UX improvements
* [ ] Translations

This is where a backend will become needed :

* [ ] Email reports
* [ ] Stats
* [ ] Integrations with 3rd party services (confluence ?)
