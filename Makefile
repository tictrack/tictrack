DOCKER_REPOSITORY=tictrack
DOCKER_IMAGE_NAME=tictrack
DOCKER_TAG=latest
DOCKER_IMAGE=${DOCKER_REPOSITORY}/${DOCKER_IMAGE_NAME}
DOCKER_REF=${DOCKER_IMAGE}:${DOCKER_TAG}

.PHONY: build
build:
	docker build --pull -t ${DOCKER_REF} .

.PHONY: tag
tag: build
	docker tag ${DOCKER_IMAGE} ${DOCKER_REF}

.PHONY: push
push: tag
	docker push ${DOCKER_REF}
