'use strict'

import Vue from 'vue'
import Vuex from 'vuex'

import participants from './modules/participants.js'
import meetings from './modules/meetings.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    participants,
    meetings
  }
})
