'use strict'

function compareMeetingTime (meeting1, meeting2) {
  if (meeting1.start > meeting2.start) {
    return 1
  } else if (meeting1.start < meeting2.start) {
    return -1
  } else return 0
}

const state = {
  meetings: []
}

const getters = {
  last5Meetings (state) {
    return state.meetings.slice().splice(-5)
  },
  latestMeeting (state) {
    return state.meetings[state.meetings.length - 1] || null
  },
  participationOverThePast5Meetings (state, getters) {
    const participantsNames = getters.last5Meetings
      .reduce((list, meeting) => {
        meeting.participants.forEach(participant => {
          if (!list.includes(participant.name)) {
            list.push(participant.name)
          }
        })

        return list
      }, [])

    const participationsSummary = participantsNames.map(name => {
      let participation = {
        name: name,
        participations: []
      }

      getters.last5Meetings.forEach(meeting => {
        if (!meeting.participants.map(participant => participant.name).includes(name)) {
          participation.participations.push(0)
        } else {
          meeting.participants.forEach(participant => {
            if (name === participant.name) {
              participation.participations.push(participant.time)
            }
          })
        }
      })

      return participation
    })

    return participationsSummary
  }
}

const mutations = {
  addMeeting (state, meeting) {
    state.meetings.push(meeting)
    state.meetings = [...state.meetings].sort(compareMeetingTime)
  },
  setMeetings (state, meetings) {
    state.meetings = meetings
  }
}

const actions = {
  loadLocalMeetings ({ commit }) {
    const savedMeetings = JSON.parse(localStorage.getItem('meetings'))

    savedMeetings.map(meeting => {
      meeting.start = new Date(meeting.start)
      meeting.end = new Date(meeting.end)

      return meeting
    })

    commit('setMeetings', savedMeetings)
  },
  save ({ commit, state }, meeting) {
    commit('addMeeting', meeting)
    localStorage.setItem('meetings', JSON.stringify(state.meetings))
  }
}

export default {
  namespaced: true,
  getters,
  state,
  mutations,
  actions
}
