'use strict'

/**
 * @returns Number
 */
function compareParticipantTime (participant1, participant2) {
  if (participant1.time < participant2.time) {
    return 1
  }
  if (participant1.time > participant2.time) {
    return -1
  }
  if (participant1.time === participant2.time) {
    return 0
  }
}

const state = {
  participants: []
}

const getters = {
  /**
   * Retrieve the participants list sorted by participation (speak time)
   */
  participantsPerParticipation (state) {
    return state.participants.slice().sort(compareParticipantTime)
  }
}

const actions = {
  getLocalParticipants ({ commit }) {
    commit('setParticipants', JSON.parse(localStorage.getItem('participants')) || [])
  },
  setLocalParticipants ({ commit }, participants) {
    localStorage.setItem('participants', JSON.stringify(participants))
    commit('setParticipants', participants)
  },
  addParticipant ({ dispatch, commit, state }, participant) {
    commit('addParticipant', participant)
    dispatch('setLocalParticipants', state.participants)
  },
  removeParticipant ({ dispatch, commit, state }, participant) {
    commit('removeParticipant', participant)
    dispatch('setLocalParticipants', state.participants)
  },
  clearParticipants ({ dispatch }) {
    dispatch('setLocalParticipants', [])
  }
}

const mutations = {
  /**
   * set the current participants list.
   */
  setParticipants (state, participants) {
    state.participants = participants
  },
  /**
   * Add a participant to the list.
   *
   * If it is the first one, it is set as default speaker.
   */
  addParticipant (state, participant) {
    if (state.participants.length === 0) {
      participant.speaking = true
    }
    state.participants.push(participant)
  },
  /**
   * removes the participant from the list.
   * Does nothing if not found.
   */
  removeParticipant (state, participantToRemove) {
    state.participants = state.participants.filter(participant => participant.name !== participantToRemove.name)
    if (state.participants.length === 1 && state.participants[0].speaking !== true) {
      state.participants[0].speaking = true
    }
  },
  /**
   * Increment by one the current speaker seconds counter
   */
  tickSpeakingParticipant (state) {
    const speaking = state.participants.find(participant => participant.speaking === true)
    if (speaking != null) {
      speaking.time++
    }
  },
  /**
   * define the current speaker
   */
  setSpeaker (state, participant) {
    if (participant.speaking === true) {
      return
    }
    state.participants.forEach(participant => { participant.speaking = false })
    participant.speaking = true
  },
  /**
   * Reset each participant time counter
   */
  resetTime (state) {
    state.participants.forEach(participant => { participant.time = 0 })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
