export default {
  methods: {
    parseTime (timeInSeconds) {
      const secs = parseInt(timeInSeconds)
      const hours = Math.floor(secs / 3600).toString().padStart(2, 0)
      const minutes = Math.floor((secs - (hours * 3600)) / 60).toString().padStart(2, 0)
      const seconds = (secs - (hours * 3600) - (minutes * 60)).toString().padStart(2, 0)

      return `${hours}:${minutes}:${seconds}`
    }
  }
}
