FROM node:latest as builder
LABEL maintainer="julien.jean@froupit.org"

WORKDIR /app

COPY package.json package-lock.json /app/

run npm ci

COPY . /app/

run npm run build --production

FROM nginx:1.15

ENV TT_HOST=localhost
ENV TT_PORT=3000

WORKDIR /app

COPY ./app.conf /etc/nginx/conf.d/default.template
COPY --from=builder /app/dist /app

EXPOSE ${TT_PORT}

CMD ["/bin/bash", "-c", "envsubst '${TT_HOST}, ${TT_PORT}' < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"]

