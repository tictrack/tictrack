import { shallowMount } from '@vue/test-utils'
import formatTimeMixin from '@/mixins/formatTimeMixin.js'

describe('formatTimeMixin.js', () => {
  describe('#parseTime', () => {
    it('should parse time', () => {
      const time = 75

      expect(formatTimeMixin.methods.parseTime(time)).toBe('00:01:15')
    })
  })
})
