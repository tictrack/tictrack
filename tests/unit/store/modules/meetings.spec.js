'use strict'

import meetingsModule from '@/store/modules/meetings.js'

describe('Meetings module', () => {
  describe(':getters', () => {
    it('#participationsOverThePast5Meetings', () => {

      const state = {}
      const mockedGetters = {
        last5Meetings: [
          {
            start: new Date(),
            end: new Date(),
            participants: [
              {
                name: 'Jack',
                time: 12
              },
              {
                name: 'Alice',
                time: 45
              },
              {
                name: 'Bob',
                time: 32
              },
            ]
          },
          {
            start: new Date(),
            end: new Date(),
            participants: [
              {
                name: 'Jack',
                time: 17
              },
              {
                name: 'Alice',
                time: 42
              },
              {
                name: 'Bob',
                time: 34
              },
            ]
          },
          {
            start: new Date(),
            end: new Date(),
            participants: [
              {
                name: 'Jack',
                time: 14
              },
              {
                name: 'Alice',
                time: 30
              },
              {
                name: 'Bob',
                time: 41
              },
              {
                name: 'NewGuy',
                time: 17
              },
            ]
          },
        ]
      }

      const expected = [
        {
          name: 'Jack',
          participations: [12, 17, 14]
        },
        {
          name: 'Alice',
          participations: [45, 42, 30]
        },
        {
          name: 'Bob',
          participations: [32, 34, 41]
        },
        {
          name: 'NewGuy',
          participations: [0, 0, 17]
        }
      ]

      const result = meetingsModule.getters.participationOverThePast5Meetings(state, mockedGetters)

      expect(result).toEqual(expected)
    })
  })
})
