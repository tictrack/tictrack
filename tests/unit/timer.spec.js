'use strict'

import { shallowMount } from '@vue/test-utils'
import Timer from '@/components/Timer.vue'

jest.useFakeTimers()

describe('Timer.vue', () => {
  describe(':props', () => {
    it('should not be starteable when disabled', () => {
      const disabled = true
      const wrapper = shallowMount(Timer, {
        propsData: { disabled }
      })
      const startButton = wrapper.find('button[type="submit"]')
      expect(startButton.attributes('disabled')).toBe('disabled')
    })
  })

  describe(':events', () => {
    it('should emit a tick event every seconds when running', () => {
      const wrapper = shallowMount(Timer)
      const startButton = wrapper.find('button[type="submit"]')

      startButton.trigger('submit')

      jest.runTimersToTime(3100)

      const evt = 'timer:tick'
      expect(evt in wrapper.emitted()).toBe(true)
      expect(wrapper.emitted()[evt].length).toBe(3)
    })
  })

  describe('behavior', () => {
    it('should display a "resume" and a "stop" button after start', () => {
      const wrapper = shallowMount(Timer)
      const startButton = wrapper.find('button[type="submit"]')

      startButton.trigger('submit')

      jest.runTimersToTime(200)
      expect(wrapper.find('button.is-info').isVisible()).toBe(true)
      expect(wrapper.find('button.is-danger').isVisible()).toBe(true)
    })
  })
})
